import LayoutWrapper from '~/layouts/LayoutWrapper.vue';

export default function (Vue, { head }) {
  Vue.component('layout-wrapper', LayoutWrapper)
}